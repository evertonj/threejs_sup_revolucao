
var camera, scene, renderer, controls;

var objects = [];

var raycaster;

var blocker = document.getElementById('blocker');
var instructions = document.getElementById('instructions');

// http://www.html5rocks.com/en/tutorials/pointerlock/intro/

var player = { height:1.8, speed:0.2, turnSpeed:Math.PI*0.02 };

var havePointerLock = 'pointerLockElement' in document || 'mozPointerLockElement' in document || 'webkitPointerLockElement' in document;

if (havePointerLock) {

    var element = document.body;

    var pointerlockchange = function (event) {

        if (document.pointerLockElement === element || document.mozPointerLockElement === element || document.webkitPointerLockElement === element) {

            controlsEnabled = true;
            controls.enabled = true;

            blocker.style.display = 'none';

        } else {

            controls.enabled = false;

            blocker.style.display = 'block';

            instructions.style.display = '';

        }

    };

    var pointerlockerror = function (event) {

        instructions.style.display = '';

    };

    // Hook pointer lock state change events
    document.addEventListener('pointerlockchange', pointerlockchange, false);
    document.addEventListener('mozpointerlockchange', pointerlockchange, false);
    document.addEventListener('webkitpointerlockchange', pointerlockchange, false);

    document.addEventListener('pointerlockerror', pointerlockerror, false);
    document.addEventListener('mozpointerlockerror', pointerlockerror, false);
    document.addEventListener('webkitpointerlockerror', pointerlockerror, false);

    instructions.addEventListener('click', function (event) {

        instructions.style.display = 'none';

        // Ask the browser to lock the pointer
        element.requestPointerLock = element.requestPointerLock || element.mozRequestPointerLock || element.webkitRequestPointerLock;
        element.requestPointerLock();

    }, false);

} else {

    instructions.innerHTML = 'Your browser doesn\'t seem to support Pointer Lock API';

}


var controlsEnabled = false;

var moveForward = false;
var moveBackward = false;
var moveLeft = false;
var moveRight = false;
var canJump = false;
var USE_WIREFRAME = false;

var prevTime = performance.now();
var velocity = new THREE.Vector3();
var direction = new THREE.Vector3();


camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 1, 1000);

scene = new THREE.Scene();
 scene.background = new THREE.Color(0xffffff);
scene.fog = new THREE.Fog(0xffffff, 0, 750);

var light = new THREE.HemisphereLight(0xeeeeff, 0x777788, 0.75);
light.position.set(0.5, 1, 0.75);
scene.add(light);



ambientLight = new THREE.AmbientLight(0xeeeeff, 0.5);
scene.add(ambientLight);

light = new THREE.PointLight(0xeeeeff, 0.8, 18, 30);
light.position.set(0, 20, 0);
light.castShadow = true;
light.shadow.camera.near = 0.9;
light.shadow.camera.far = 25;
scene.add(light);



mesh = new THREE.Mesh(
    new THREE.BoxGeometry(1,1,1),
    new THREE.MeshPhongMaterial({color:0xff4444, wireframe:USE_WIREFRAME})
);
mesh.position.y += 1;
// The cube can have shadows cast onto it, and it can cast shadows
mesh.receiveShadow = true;
mesh.castShadow = true;
scene.add(mesh);

meshFloor = new THREE.Mesh(
    new THREE.PlaneGeometry(10,10,10,10),
    // MeshBasicMaterial does not react to lighting, so we replace with MeshPhongMaterial
    new THREE.MeshPhongMaterial({color:0xffffff, wireframe:USE_WIREFRAME})
    // See threejs.org/examples/ for other material types
);
meshFloor.rotation.x -= Math.PI / 2;
// Floor can have shadows cast onto it
meshFloor.receiveShadow = true;
scene.add(meshFloor);


// LIGHTS
ambientLight = new THREE.AmbientLight(0xffffff, 0.2);
scene.add(ambientLight);

light = new THREE.PointLight(0xffffff, 0.8, 18);
light.position.set(-3,6,-3);
light.castShadow = true;
// Will not light anything closer than 0.1 units or further than 25 units
light.shadow.camera.near = 0.1;
light.shadow.camera.far = 25;
scene.add(light);


camera.position.set(0, player.height, -5);
camera.lookAt(new THREE.Vector3(0,player.height,0));

 renderer = new THREE.WebGLRenderer();
 renderer.setSize(1280, 720);

// Enable Shadows in the Renderer
renderer.shadowMap.enabled = true;
renderer.shadowMap.type = THREE.BasicShadowMap;

document.body.appendChild(renderer.domElement);



controls = new THREE.PointerLockControls(camera);

scene.add(controls.getObject());

var onKeyDown = function (event) {

    switch (event.keyCode) {

        case 38: // up
        case 87: // w
            moveForward = true;
            break;

        case 37: // left
        case 65: // a
            moveLeft = true;
            break;

        case 40: // down
        case 83: // s
            moveBackward = true;
            break;

        case 39: // right
        case 68: // d
            moveRight = true;
            break;

        case 32: // space
            if (canJump === true) velocity.y += 350;
            canJump = false;
            break;

    }

};

var onKeyUp = function (event) {

    switch (event.keyCode) {

        case 38: // up
        case 87: // w
            moveForward = false;
            break;

        case 37: // left
        case 65: // a
            moveLeft = false;
            break;

        case 40: // down
        case 83: // s
            moveBackward = false;
            break;

        case 39: // right
        case 68: // d
            moveRight = false;
            break;

    }

};

document.addEventListener('keydown', onKeyDown, false);
document.addEventListener('keyup', onKeyUp, false);

raycaster = new THREE.Raycaster(new THREE.Vector3(), new THREE.Vector3(0, -1, 0), 0, 10);

// floor

var floorGeometry = new THREE.PlaneGeometry(2000, 2000, 100, 100);
floorGeometry.rotateX(-Math.PI / 2);

for (var i = 0, l = floorGeometry.vertices.length; i < l; i++) {

    var vertex = floorGeometry.vertices[i];
    vertex.x += Math.random() * 20 - 10;
    vertex.y += Math.random() * 2;
    vertex.z += Math.random() * 20 - 10;

}

for (var i = 0, l = floorGeometry.faces.length; i < l; i++) {

    var face = floorGeometry.faces[i];
    face.vertexColors[0] = new THREE.Color().setRGB(0, 0, 0); //setHSL( Math.random() * 0.3 + 0.5, 0.75, Math.random() * 0.25 + 0.75 );
    face.vertexColors[1] = new THREE.Color().setRGB(0, 0, 255); //setHSL( Math.random() * 0.3 + 0.5, 0.75, Math.random() * 0.25 + 0.75 );
    face.vertexColors[2] = new THREE.Color().setRGB(20, 20, 20); //setHSL( Math.random() * 0.3 + 0.5, 0.75, Math.random() * 0.25 + 0.75 );
    face.vertexColors[ 1 ] = new THREE.Color().setHSL( Math.random() * 0.3 + 0.5, 0.75, Math.random() * 0.25 + 0.75 );
    face.vertexColors[ 2 ] = new THREE.Color().setHSL( Math.random() * 0.3 + 0.5, 0.75, Math.random() * 0.25 + 0.75 );

}

var floorMaterial = new THREE.MeshBasicMaterial({vertexColors: THREE.VertexColors});

var floor = new THREE.Mesh(floorGeometry, floorMaterial);
scene.add(floor);

// Models index
var models = {
    tent: {
        obj: "models/Tent_Poles_01.obj",
        mtl: "models/Tent_Poles_01.mtl",
        mesh: null
    },
    campfire: {
        obj: "models/Campfire_01.obj",
        mtl: "models/Campfire_01.mtl",
        mesh: null
    },
    pirateship: {
        obj: "models/Pirateship.obj",
        mtl: "models/Pirateship.mtl",
        mesh: null
    },
    uzi: {
        obj: "models/uziGold.obj",
        mtl: "models/uziGold.mtl",
        mesh: null,
        castShadow: false
    }
};

// Meshes index
var meshes = {};


function onResourcesLoaded() {

    // Clone models into meshes.
    meshes["tent1"] = models.tent.mesh.clone();
    meshes["tent2"] = models.tent.mesh.clone();
    meshes["campfire1"] = models.campfire.mesh.clone();
    meshes["campfire2"] = models.campfire.mesh.clone();
    meshes["pirateship"] = models.pirateship.mesh.clone();

    // Reposition individual meshes, then add meshes to scene
    meshes["tent1"].position.set(-125, 1, 40);
    meshes["tent1"].scale.set(15, 15, 15);

    scene.add(meshes["tent1"]);

    meshes["tent2"].position.set(-200, 1, 40);
    meshes["tent2"].scale.set(15, 15, 15);
    scene.add(meshes["tent2"]);

    meshes["campfire1"].position.set(-40, 1, 10);
    meshes["campfire1"].scale.set(15, 15, 15);
    meshes["campfire2"].position.set(-80, 1, 10);
    meshes["campfire2"].scale.set(15, 15, 15);

    scene.add(meshes["campfire1"]);
    scene.add(meshes["campfire2"]);

    meshes["pirateship"].position.set(-300, 1, 10);
    meshes["pirateship"].rotation.set(0, Math.PI, 0); // Rotate it to face the other way.
    meshes["pirateship"].scale.set(15, 15, 15);
    scene.add(meshes["pirateship"]);

    // player weapon
    meshes["playerweapon"] = models.uzi.mesh.clone();
    meshes["playerweapon"].position.set(0, 7, 0);
    meshes["playerweapon"].scale.set(100, 100, 100);
    scene.add(meshes["playerweapon"]);

}



loadingManager = new THREE.LoadingManager();
loadingManager.onProgress = function (item, loaded, total) {
    console.log(item, loaded, total);
};
loadingManager.onLoad = function () {
    console.log("loaded all resources");
    RESOURCES_LOADED = true;
    onResourcesLoaded();
};

var textureLoader = new THREE.TextureLoader(loadingManager);
crateTexture = textureLoader.load("crate0/crate0_diffuse.jpg");
crateBumpMap = textureLoader.load("crate0/crate0_bump.jpg");
crateNormalMap = textureLoader.load("crate0/crate0_normal.jpg");

crate = new THREE.Mesh(
    new THREE.BoxGeometry(30, 30, 30),
    new THREE.MeshPhongMaterial({
        color: 0xffffff,
        map: crateTexture,
        bumpMap: crateBumpMap,
        normalMap: crateNormalMap
    })
);
scene.add(crate);
crate.position.set(60, 16, 10);
crate.receiveShadow = true;
crate.castShadow = true;


for (var _key in models) {
    (function (key) {

        var mtlLoader = new THREE.MTLLoader(loadingManager);
        mtlLoader.load(models[key].mtl, function (materials) {
            materials.preload();

            var objLoader = new THREE.OBJLoader(loadingManager);

            objLoader.setMaterials(materials);
            objLoader.load(models[key].obj, function (mesh) {

                mesh.traverse(function (node) {
                    if (node instanceof THREE.Mesh) {
                        if ('castShadow' in models[key])
                            node.castShadow = models[key].castShadow;
                        else
                            node.castShadow = true;

                        if ('receiveShadow' in models[key])
                            node.receiveShadow = models[key].receiveShadow;
                        else
                            node.receiveShadow = true;
                    }
                });
                models[key].mesh = mesh;

            });
        });

    })(_key);
}



    var boxGeometry = new THREE.BoxGeometry( 20, 20, 20 );

    for ( var i = 0, l = boxGeometry.faces.length; i < l; i ++ ) {

        var face = boxGeometry.faces[ i ];
        face.vertexColors[ 0 ] = new THREE.Color().setHSL( Math.random() * 0.3 + 0.5, 0.75, Math.random() * 0.25 + 0.75 );
        face.vertexColors[ 1 ] = new THREE.Color().setHSL( Math.random() * 0.3 + 0.5, 0.75, Math.random() * 0.25 + 0.75 );
        face.vertexColors[ 2 ] = new THREE.Color().setHSL( Math.random() * 0.3 + 0.5, 0.75, Math.random() * 0.25 + 0.75 );

    }

    for ( var i = 0; i < 500; i ++ ) {

        var boxMaterial = new THREE.MeshPhongMaterial( { specular: 0xffffff, flatShading: true, vertexColors: THREE.VertexColors } );
        boxMaterial.color.setHSL( Math.random() * 0.2 + 0.5, 0.75, Math.random() * 0.25 + 0.75 );

        var box = new THREE.Mesh( boxGeometry, boxMaterial );
        box.position.x = Math.floor( Math.random() * 20 - 10 ) * 20;
        box.position.y = Math.floor( Math.random() * 20 ) * 20 + 10;
        box.position.z = Math.floor( Math.random() * 20 - 10 ) * 20;

        scene.add( box );
        objects.push( box );

    }



//renderer = new THREE.WebGLRenderer();
renderer.setPixelRatio(window.devicePixelRatio);
renderer.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild(renderer.domElement);

//

window.addEventListener('resize', onWindowResize, false);


function onWindowResize() {

    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize(window.innerWidth, window.innerHeight);

}


function mouseMove(event) {


}

function animate() {

    requestAnimationFrame(animate);

    if (controlsEnabled === true) {

        raycaster.ray.origin.copy(controls.getObject().position);
        raycaster.ray.origin.y -= 10;

        var intersections = raycaster.intersectObjects(objects);

        var onObject = intersections.length > 0;

        var time = performance.now();
        var delta = ( time - prevTime ) / 1000;

        velocity.x -= velocity.x * 10.0 * delta;
        velocity.z -= velocity.z * 10.0 * delta;

        velocity.y -= 9.8 * 100.0 * delta; // 100.0 = mass

        direction.z = Number(moveForward) - Number(moveBackward);
        direction.x = Number(moveLeft) - Number(moveRight);
        direction.normalize(); // this ensures consistent movements in all directions

        if (moveForward || moveBackward) velocity.z -= direction.z * 400.0 * delta;
        if (moveLeft || moveRight) velocity.x -= direction.x * 400.0 * delta;

        if (onObject === true) {

            velocity.y = Math.max(0, velocity.y);
            canJump = true;

        }

        controls.getObject().translateX(velocity.x * delta);
        controls.getObject().translateY(velocity.y * delta);
        controls.getObject().translateZ(velocity.z * delta);

        meshes["playerweapon"].position.x = controls.getObject().position.x;
        meshes["playerweapon"].position.y = 7;//controls.getObject().position.y - 2.5;
        meshes["playerweapon"].position.z = controls.getObject().position.z;


        if (controls.getObject().position.y < 10) {

            velocity.y = 0;
            controls.getObject().position.y = 10;

            canJump = true;

        }

        prevTime = time;

    }

    renderer.render(scene, camera);


}

//document.addEventListener( 'mousemove', mouseMove, false );


// init();
animate();
